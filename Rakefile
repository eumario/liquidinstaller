# Liquid Installer, version 0.1 Alpha
# Copyright (C) 2012 Mario Steele
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detaisl.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# You may contact the original author via email at mario@ruby-im.net


$: << "."
require 'io/console'
require 'pty'

def shell_exec(cmd, args, &block)
  m,s = PTY.open
  s.raw!
  r, w = IO.pipe
  pid = spawn("#{cmd} #{args}", :in => r, :out => s)
  r.close
  s.close
  data = ""
  loop do
  	begin
      data += m.read_nonblock(1024)
      while data.index("\n")
        line = data[0..data.index("\n")-1]
        data = data[data.index("\n")+1..-1]
        yield line if block_given?
        @keepalive_hook.call
      end
  	rescue IO::WaitReadable
  		IO.select([m],[],[],100)
  		retry
  	rescue EOFError
  		break
  	rescue
  		break
  	end
  end
	w.close
	m.close
end

namespace :gui do
	desc "Generate XRC Wrapper file"
	task :generate do
		`xrcise -n LiquidInstaller -o lib/liquidInstaller/gui/LiquidGUI.rb share/liquidInstaller/LiquidGUI.xrc`
		puts "XRC Wrapper File generated"
	end

	desc "Executes the GUI Frontend for the Installer"
	task :run do
		if Process.uid != 0
      shell_exec("sudo","rake gui:run") do |line|
        puts line
      end
		else
      shell_exec("ruby","-I #{Dir.pwd} lib/liquidInstaller/gui/wxgui.rb") do |line|
        puts line
      end
		end
	end
  
  desc "Cleanup mounts from an aborted installation test run"
  task :cleanup do
    `sudo umount /mnt/target/proc`
    `sudo umount /mnt/target/sys`
    `sudo umount /mnt/target/dev/pts`
    `sudo umount /mnt/target/dev/shm`
    `sudo umount /mnt/target/dev`
    `sudo umount /mnt/target`
    `sudo umount /mnt/source`
    `sudo umount /mnt/.squashfs`
    `sudo swapoff /dev/sda2`
  end

	task :parted do
		puts `ruby -I "#{Dir.pwd}" lib/liquidInstaller/gui/partition_editor.rb`
	end
end

namespace :irb do
	require 'irb'
	require 'irb/completion'
	desc "Run IRB for testing of Core Library"
	task :run do
		ARGV.clear
		ARGV << "-I" << "'#{Dir.pwd}'" << "-r" << "lib/liquidInstaller/core/utils" << "-r" << "lib/liquidInstaller/core/installer"
		IRB.start
	end

	desc "Run IRB for testing Parted Library"
	task :parted do
		ARGV.clear
		ARGV << "-I" << "'#{Dir.pwd}'" << "-r" << "lib/liquidInstaller/core/rubyparted"
		IRB.start
	end
end

namespace :package do
  desc "Builds a Arch Linux package for use with pacman"
  task :lemur do
    shell_exec("makepkg","") do |line|
      puts line
    end
    rm_rf "src"
    rm_rf "pkg"
  end
end

desc "Install the package"
task :install do
  prefix = "/usr"
  destdir = ENV["DESTDIR"].nil? ? "" : ENV["DESTDIR"]
  prefix = File.join(destdir,prefix)
  puts "Prefix: #{prefix}"
  mkdir_p File.join(destdir,"etc","skel","Desktop")
  mkdir_p File.join(prefix,"bin")
  mkdir_p File.join(prefix,"share","applications")
  mkdir_p File.join(prefix,"share","pixmaps")
  install("etc/liquidInstaller.conf",File.join(destdir,"etc"))
  install("bin/liquidInstaller",File.join(prefix,"bin"),:mode=>0755)
  cp_r "lib", File.join(prefix,"lib")
  cp_r "share/liquidInstaller", File.join(prefix,"share/")
  cp "share/liquidInstaller/images/liquidInstaller.png", File.join(prefix,"share","pixmaps")
  File.open(File.join(prefix,"share","applications","liquidInstaller.desktop"),"w") do |fh|
    fh.puts <<EOF
[Desktop Entry]
Categories=GNOME;System;
Comment=Install Liquid Lemur Linux
Exec=/usr/bin/liquidInstaller
GenericName=Install Liquid Lemur Linux
Icon=/usr/share/pixmaps/liquidInstaller.png
Name=Install Liquid Lemur Linux
StartupNotify=true
Terminal=false
Type=Application
Version=0.9
EOF
  end
  cp File.join(prefix,"share","applications","liquidInstaller.desktop"), File.join(destdir,"etc","skel","Desktop")
end
