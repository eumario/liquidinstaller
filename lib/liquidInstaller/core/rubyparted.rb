# Liquid Installer, version 0.1 Alpha
# Copyright (C) 2012 Mario Steele
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detaisl.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# You may contact the original author via email at mario@ruby-im.net


module Parted
  # Module Functions
  class << self
    def probeDevices
      %x(ls /sys/block).split("\n").grep(/sd|hd/).collect {|x| x.chomp}
    end

    def probeDisks
      probeDevices.collect {|x| Disk.new(x)}
    end
  end

  class Partition
    attr_reader :path, :dev, :size, :uuid, :label, :type, :nextPartition
    attr_accessor :mount, :format_as
    def initialize(disk,dev)
      part = %x(cat /proc/partitions | grep "#{dev}" | awk '{print $3" "$4}').strip.split
      @path = "/dev/#{dev}"
      @dev = disk
      @size = part[0].to_i / 1024
      @uuid = %x(blkid -o value -s UUID /dev/"#{dev}").strip
      @label = %x(blkid -o value -s LABEL /dev/"#{dev}").strip
      @type = %x(blkid -o value -s TYPE /dev/"#{dev}").strip
      @mount = ""
      @format = false
      @format_as = ""
      @nextPartition = nil
    end

    class << self
      def freeSpacePartition(disk,used)
        part = self.allocate()
        part.instance_variable_set(:@dev,disk)
        part.instance_variable_set(:@size,used)
        part.instance_variable_set(:@type,"unallocated")
        part.instance_variable_set(:@mount,"")
        part
      end
    end

    def format?
      @format
    end

    def format=(value)
      @format = value
    end

    def nextPartition=(value)
      return unless @nextPartition.nil?
      @nextPartition = value
    end

    def has_label?
      not @label.empty?
    end
  end

  class Disk
    attr :device, :size, :vendor, :path
    def initialize(dev)
      if dev.index("/")
        @path = dev
        @device = dev.split("/")[-1]
      else
        @device = dev
        @path = "/dev/#{dev}"
      end
      data = %x(parted -s /dev/"#{@device}" unit MB p | grep /dev/"#{@device}").split("\n")
      if data[0] =~ /unrecognised disk label/
        fresh_disk
      end
      lsize = %x(parted -s /dev/"#{@device}" unit MB p | grep /dev/"#{@device}").split[2]
      @size = lsize.nil? ? 0 : lsize[0..-3].to_i
      @removable = (IO.read("/sys/block/#{@device}/removable").chomp.to_i == 1)
      @vendor = IO.read("/sys/block/#{@device}/device/vendor").strip
    end

    def removable?
      @removable
    end

    def getFirstPartition()
      used = 0
      if @first_partition.nil?
        parts = %x(cat /proc/partitions | grep "#{@device}" | awk '{print $4}').strip.split
        last_partition = nil
        parts.each do |part|
          partition = Partition.new(self,part) unless part == @device
          @first_partition = partition if @first_partition.nil?
          last_partition.nextPartition = partition unless last_partition.nil?
          last_partition = partition
          used += partition.size unless partition.nil?
        end
      end
      if used < @size
        part = Partition.freeSpacePartition(self,@size - used)
        @first_partition = part if @first_partition.nil?
        last_partition.nextPartition = part unless last_partition.nil?
      end
      @first_partition
    end

    def refreshPartitions()
      @first_partition = nil
    end

    def fresh_disk
      `parted -s #{@path} mklabel msdos`
    end

    def create_swap(size,start)
      `parted -s #{@path} mkpart primary linux-swap #{start}MB #{start+size}MB`
    end

    def create_partition(size,start)
      `parted -s #{@path} mkpart primary #{start}MB #{start+size}MB`
    end
  end
end
