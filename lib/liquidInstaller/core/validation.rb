# Liquid Installer, version 0.1 Alpha
# Copyright (C) 2012 Mario Steele
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detaisl.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# You may contact the original author via email at mario@ruby-im.net


module LiquidInstaller
  module Validation
    class << self
      ### Code below taken from Ubuntu Installer Ubiquity
      def password_strength(pass)
        upper = lower = digit = symbol = 0
        pass.each_char do |char|
          if char =~ /\d/
            digit += 1
          elsif char =~ /[a-z]/
            lower += 1
          elsif char =~ /[A-Z]/
            upper += 1
          else
            symbol += 1
          end
        end
        length = pass.length
        length = 5 if length > 5
        digit = 3 if length > 3
        upper = 3 if upper > 3
        symbol = 3 if symbol > 3
        strength = (((length * 0.1) - 0.2) + (digit * 0.1) + (symbol * 0.15) + (upper * 0.1))
        strength = 1 if strength > 1
        strength = 0 if strength < 0
        return strength
      end

      def human_password_strength(password)
        strength = password_strength(password)
        length = password.length
        if length == 0
          hint = ''
          color = ''
        elsif length < 6
          hint = "too_short"
          color = "#8B0000"  # darkred
        elsif strength < 0.5
          hint = "weak"
          color = "#8B0000"  # darkred
        elsif strength < 0.75
          hint = "fair"
          color = "#FF8C00"  # darkorange
        elsif strength < 0.9
          hint = "good"
          color = "#006400"  # darkgreen
        else
          hint = "strong"
          color = "#006400"  # darkgreen
        end
        return [hint,color]
      end

      def validate_username(username)
        if (username[0] =~ /[a-z]/).nil?
          return "first_lowercase"
        elsif username.match(/^[-a-z0-9_]+/)[0] != username
          return "invalid_symbols"
        else
          return ""
        end
      end

      def validate_hostname(hostname)
        if hostname.length < 1 or hostname.length > 63
          return "invalid_length"
        end
        if not hostname =~ /^[a-zA-Z0-9.-]+/
          return "bad_char"
        end
        if hostname[0] == "-" or hostname[-1] == "-"
          return "bad_hyphen"
        end
        if hostname.index("..") or hostname[0] == "." or hostname[0] == "."
          return "bad_dots"
        end
        return ""
      end
    end
  end
end
