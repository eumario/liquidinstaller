# Liquid Installer, version 0.1 Alpha
# Copyright (C) 2012 Mario Steele
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detaisl.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# You may contact the original author via email at mario@ruby-im.net

module LiquidInstaller
  class Config
    class << self
      def init
        dir = File.expand_path(File.dirname(__FILE__))
        @@prefix = File.expand_path(File.join(dir,"..","..",".."))
      end

      def is_sandbox?
        (@@prefix != "/usr")
      end

      def prefix(*path)
        return File.join(@@prefix,*path)
      end
    end
  end
end

LiquidInstaller::Config.init

if $0 == __FILE__
  p LiquidInstaller::Config.is_sandbox?
  p LiquidInstaller::Config.prefix("share/liquidInstaller/LiquidGUI.xrc")
end
