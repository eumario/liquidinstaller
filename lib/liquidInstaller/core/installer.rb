# Liquid Installer, version 0.1 Alpha
# Copyright (C) 2012 Mario Steele
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detaisl.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# You may contact the original author via email at mario@ruby-im.net


require 'yaml'          # For Configuration opening
require 'yaml'          # For Configuration opening
require 'fileutils'     # For File.cp, mkdir, etc, etc
require 'pty'           # For shell_exec
require 'io/console'    # For shell_exec
require 'thread'

require 'lib/liquidInstaller/core/config'
require 'lib/liquidInstaller/core/rubyparted'

Thread.abort_on_exception = true
include FileUtils

module LiquidInstaller
  ###DOC: Regular expressions for gathering data from command line tools
  REGEXP_DRV = /\d: \/dev\/([sd|hd]\w+) \w+ ([\d|.]+)(\w+)/
  REGEXP_UMOUNT_PART = /ID: \/dev\/([\w|\d]+) size: ([\d|.]+)(\w+) label: ([\w|\d|\s]+) /
  REGEXP_MOUNT_PART = /ID: ([\w|\d|\S]+) size: ([\d|.]+)(\w+) used: ([\d|.]+)(\w+) .+ fs: ([\w|\d]+) dev: \/dev\/([\w|\d]+)/
  SEMAPHORE = Mutex.new


  # Holds User Configuration choices
  class Setup
    attr_accessor :language, :timezone, :keyboard, :disk, :partition_table, :user_info, :bootloader, :advanced_partition, :root
    def initialize()
      @language = "en_US"
      @timezone = {:timezone=>"", :timezone_code=>""}
      ##NOTE @keyboard => {:model, :model_desc, :layout, :layout_desc, :variant, :variant_desc }
      @keyboard = {}
      ##NOTE @partition_table => {:devid => {:format_type => "fmt", :mount_point => "path", :format => bool}}
      @partition_table = {}
      ##NOTE @disk => "devid"
      @disk = "sda"
      ##NOTE @user_info => {:realname,:username,:pass1,:pass2,:hostname,:autologin}
      @user_info = {}
      ##NOTE @bootloader => {:devid, :install}
      @bootloader = {}
      ##NOTE @advanced_partition => true|false
      @advanced_partition = false
      ##NOTE @root => {"password","password_confirm"}
      @root = {}
    end
  end

  # Core Installer System
  class Installer
    attr_reader :distro_name, :distro_vers, :distro_candidate, :live_user, :media, :media_type
    include FileUtils
    def initialize
      if Config.is_sandbox?
        @conf_file = Config.prefix("etc/liquidInstaller.conf")
      else
        @conf_file = '/etc/liquidInstaller.conf'
      end
      cfg = YAML.load(File.open(@conf_file))
      @distro_name = cfg[:distribution][:name]
      @distro_vers = cfg[:distribution][:version]
      @distro_candidate = cfg[:distribution][:candidate]
      @live_user = cfg[:install][:live_user]
      @media = cfg[:install][:source]
      @media_type = cfg[:install][:type]
    end

    ### DOC: This sets the callback to be called on progress updates
    ### Callback Signature: my_callback(progress_type, message, current_progress, total)
    ### Where progress_type is any of :start, :update, :complete, :error
    def progress_hook(&proc)
      @progress_hook = proc
    end

    ### DOC: Sets the callback to be called on errors
    def error_hook(&proc)
      @error_hook = proc
    end

    ### DOC: Calls forth a Message Box to display a question
    ### Callback Signature: question_box(title,message,buttons)
    ### buttons: [:yesno, :ok, :cancel]
    def question_box(&proc)
      @question_box = proc
    end
    
    def keep_alive(&proc)
      @keepalive_hook = proc
    end

    ### DOC: Main entry point to start the installer, with user provided parameters
    def run_install(setupInst)
      @setup = setupInst
      _run_installer
    end

    private
    # Methods only used in the Installer core

    def progress(type,msg,current=0,total=100)
      @progress_hook.call(type,msg,current,total)
    end
    
    def shell_exec(cmd, args, skip_lines=false, &block)
  		m,s = PTY.open
  		s.raw!
  		r, w = IO.pipe
  		pid = spawn("#{cmd} #{args}", :in => r, :out => s)
  		r.close
  		s.close
      data = ""
  		loop do
  			begin
          data += m.read_nonblock(1024)
          while data.index("\n")
            line = data[0..data.index("\n")-1]
            data = data[data.index("\n")+1..-1]
            if skip_lines && data.index("\n")
              next
            end
            yield line if block_given?
            @keepalive_hook.call
          end
  			rescue IO::WaitReadable
  				IO.select([m],[],[],10)
          @keepalive_hook.call
  				retry
  			rescue EOFError
  				break
  			rescue
  				break
  			end
  		end
  		w.close
  		m.close
    end
    
    def chroot(cmd)
      shell_exec("chroot","/mnt/target /bin/bash -c \"#{cmd}\"")
    end

    ### DOC: Creates partitions in @setup, calculating what is needed
    def _create_partitions()
      total_mem = `cat /proc/meminfo | grep MemTotal | awk '{print $2}'`.strip.to_i
      swap_size = (total_mem * 1.5).round / 1024
      disk = Parted::Disk.new(@setup.disk)
      disk.fresh_disk
      space = (disk.size - swap_size) - 1 # Adjust for Swap Size, and MBR Post-Gap
      if @setup.partition_table.length == 3
        root = 7680
        home = space - root
        progress(:pulse,"Creating partition /")
        disk.create_partition(root,1)
        progress(:pulse,"Creating partition /home")
        disk.create_partition(home,root+1)
        progress(:pulse,"Creating swap partition...")
        disk.create_swap(swap_size,root+home+1)
      else
        progress(:pulse,"Creating partition /")
        disk.create_partition(space,1)
        progress(:pulse,"Creating swap partition...")
        disk.create_swap(swap_size,space+1)
      end
      `partprobe -d #{disk.path}`
    end

    ### DOC: Formats partitions in @setup, based upon values provided.
    def _format_partitions()
      _create_partitions unless @setup.advanced_partition
      @setup.partition_table.each do |dev,info|
        if %w(ext2 ext3 ext4 jfs reiserfs).include? info[:format_type]
          option = "-q"
        elsif %w(xfs).include? info[:format_type]
          option = "-f -q"
        else
          option = ""
        end
        if info[:format_type] != "swap"
          cmd = "mkfs.#{info[:format_type]}"
        else
          cmd = "mkswap"
        end
        if info[:format]
          progress(:pulse,"Formatting #{dev} as #{info[:format_type]}...")
          shell_exec(cmd,"#{option} #{dev}")
        end
      end
    end

    def _mount_partitions()
      @setup.partition_table.each do |dev,info|
        if info[:format_type] == "swap"
          `swapon #{dev}`
        else
          path = File.join(@target,info[:mount_point])
          `mount -t #{info[:format_type]} #{dev} #{path}`
        end
      end
      rm_rf File.join(@target,"/lost+found")  ## Don't need lost+found in root
    end

    def _copy_files()
      progress(:pulse,"Indexing files...")
      filelist = []
      shell_exec("rsync","-avhn #{@source}/ #{@target}/") do |data|
        filelist << data
      end
      total = filelist.length
      progress(:pulse_stop,"Copying files...",0,total)
      shell_exec("rsync","-avh #{@source}/ #{@target}/",true) do |data|
        data.strip!
        progress(:update,"Copying /#{data}...",filelist.index(data),filelist.length)
      end
    end
    
    def _mk_sys
  		mkdir_p File.join(@target,"dev","shm") unless Dir.exists? File.join(@target,"dev","shm")
	  	mkdir_p File.join(@target,"dev","pts") unless Dir.exists? File.join(@target,"dev","pts")
		  mkdir_p File.join(@target,"sys") unless Dir.exists? File.join(@target,"sys")
  		mkdir_p File.join(@target,"proc") unless Dir.exists? File.join(@target,"proc")
    end

    def _mount_sys
      _mk_sys
      `mount --bind /dev/ #{@target}/dev/`
      `mount --bind /dev/shm #{@target}/dev/shm`
      `mount --bind /dev/pts #{@target}/dev/pts`
      `mount --bind /sys/ #{@target}/sys/`
      `mount --bind /proc/ #{@target}/proc/`
    end

    def _umount_sys
      `umount #{@target}/dev/`
      `umount #{@target}/dev/shm`
      `umount #{@target}/dev/pts`
      `umount #{@target}/sys/`
      `umount #{@target}/proc/`
    end

    def _run_installer
      ##TODO Setup installation workflow.
      progress(:start,"Initializing Installer")
      squashfs = "/run/archiso/bootmnt/lemur/%s/root-image.fs.sfs" % `arch`.strip
      @source = "/mnt/source"
      @target = "/mnt/target"
      if not Dir.exists? "/mnt/.squashfs"
        mkdir "/mnt/.squashfs"
      end
      if not Dir.exists? @source
        mkdir @source
      end
      if not Dir.exists? @target
        mkdir @target
      end
      `mount #{squashfs} /mnt/.squashfs -t squashfs -o loop`
      `mount /mnt/.squashfs/root-image.fs /mnt/source -t ext4 -o loop`
      progress(:pulse_start,"Formatting Partitions...")
      _format_partitions
      _mount_partitions
      _copy_files
      _mount_sys
      cp "/etc/resolv.conf","#{@target}/etc/resolv.conf"
      progress(:pulse_start,"Removing live user")
      chroot("userdel %s" % self.live_user)
      if (File.exists? "#{@target}/home/%s" % self.live_user)
        chroot("rm -rf /home/%s" % self.live_user)
      end
      progress(:pulse,"Adding new user")
      chroot("useradd -s %s -c '%s' -G users -m %s" % ["/bin/bash",@setup.user_info[:realname], @setup.user_info[:username]])
      newusers = File.open("#{@target}/tmp/newusers.conf","w")
      newusers.puts("%s:%s" % [@setup.user_info[:username],@setup.user_info[:pass1]])
      newusers.puts("root:%s" % [@setup.root[:password]])
      newusers.close()
      chroot("cat /tmp/newusers.conf | chpasswd")
      rm_rf "#{@target}/tmp/newusers.conf"
      rm_rf "#{@target}/lost+found"
      chroot("usermod -a -G wheel %s" % [@setup.user_info[:username]])
      chroot("usermod -a -G games %s" % [@setup.user_info[:username]])
      chroot("usermod -a -G storage %s" % [@setup.user_info[:username]])
      chroot("usermod -a -G power %s" % [@setup.user_info[:username]])
      progress(:pulse,"Adding filesystems to mount...")
      if not File.exists?("#{@target}/etc/fstab")
        `echo "#### Static Filesystem Table File" > #{@target}/etc/fstab`
      end
      File.open("#{@target}/etc/fstab","a") do |fh|
        @setup.partition_table.each do |dev,info|
          fh.puts("%s\t%s\t%s\tdefaults\t0\t0" % [dev,info[:mount_point],info[:format_type]])
        end
      end
      progress(:pulse,"Setting up Locale...")
      data = IO.read("#{@target}/etc/rc.conf")
      File.open("#{@target}/etc/rc.conf","wb") do |fh|
        data.each_line do |line|
          if line.strip =~ /^HOSTNAME=/
            fh.puts "HOSTNAME=\"#{@setup.user_info[:hostname]}\""
          elsif line.strip =~ /^LOCALE=/
            fh.puts "LOCALE=\"#{@setup.language}.UTF-8\""
          elsif line.strip =~ /^TIMEZONE=/
            fh.puts "TIMEZONE=\"#{@setup.timezone[:timezone]}\""
          else
            fh.print line
          end
        end
      end
      cp "#{@target}/usr/share/zoneinfo/%s" % [@setup.timezone[:timezone]], "#{@target}/etc/localtime"
      progress(:pulse,"Setting Keyboard...")
      File.open("#{@target}/etc/lxdm/LoginReady","w") do |fh|
        fh.puts("#!/bin/sh")
        fh.puts("setxkbmap -model #{@setup.keyboard[:model]}")
        fh.puts("setxkbmap -layout #{@setup.keyboard[:layout]}")
        fh.puts("setxkbmap -variant #{@setup.keyboard[:variant]}") unless @setup.keyboard[:variant].nil? or @setup.keyboard[:variant].empty?
      end
      progress(:pulse,"Generating Kernel Image for boot...")
      chroot("mkinitcpio -p linux")
      if @setup.bootloader[:install]
        progress(:pulse,"Installing Burg...")
        chroot("burg-install --force %s" % @setup.bootloader[:devid])
        progress(:pulse,"Configuring Burg...")
        chroot("burg-mkconfig -o /boot/burg/burg.cfg")
      end
      data = IO.read("#{@target}/etc/lxdm/lxdm.conf")
      File.open("#{@target}/etc/lxdm/lxdm.conf","w") do |fh|
        data.each_line do |line|
          if line.strip =~ /^autologin=lemur/
            if @setup.user_info[:autologin]
              fh.puts("autologin=#{@setup.user_info[:username]}")
            end
          else
            fh.print(line)
          end
        end
      end
      
      _umount_sys
      @setup.partition_table.each do |dev,info|
        `umount #{dev}` unless (info[:mount_point] == '/')
      end
      `umount #{@target}`
      `umount #{@source}`
      `umount /mnt/.squashfs`
      progress(:complete,"Installation successful!")
    end
  end

end

