# Liquid Installer, version 0.1 Alpha
# Copyright (C) 2012 Mario Steele
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detaisl.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# You may contact the original author via email at mario@ruby-im.net


module LiquidInstaller
  class LIWizardPage
    attr_reader :widget
    def initialize(widget, sizer)
      @widget = widget
      @sizer = sizer
      @hidden = true
      @widget.hide
      @skip = false
    end

    def is_hidden?
      return @hidden
    end

    def show
      @sizer.add(@widget,2,Wx::ALL|Wx::EXPAND)
      @widget.show
      @hidden = false
      @sizer.layout
      Wx::Timer.after(500) do
        @widget.refresh
      end
    end

    def hide
      @sizer.detach(@widget)
      @widget.hide
      @hidden = true
    end

    def skip
      @skip = true
    end

    def unskip
      @skip = false
    end

    def skip?
      @skip
    end
  end

  class LIWizard
    def initialize(bprev,bnext,&wizard_end)
      @pages = []
      @registry = {}
      @step = 0
      @manual_step = false
      @bprev = bprev
      @bprev.enable(false)
      @bnext = bnext
      @wizard_end = nil
      @prev_text = "<< &Previous"
      @next_text = "&Next >>"
      @finish_text = "&Finish"
      @proc_collect = nil
    end
    attr_reader :step, :pages
    attr_accessor :wizard_end, :proc_collect, :step_change
    attr_accessor :prev_text, :next_text, :finish_text

    def add_page(widget,label=nil)
      @pages << widget
      @registry[label] = widget unless label.nil?
    end

    def [](label)
      if label.class == Symbol
        return @registry[label]
      else
        return @pages[label]
      end
    end

    def step=(val)
      @step = val
      @manual_step = true
    end

    def init_page()
      @step = 0
      @pages[@step].show
      @step_change.call(@step) unless @step_change.nil?
    end

    def next_page()
      if @step == @pages.length - 1
        @wizard_end.call(self) unless @wizard_end.nil?
        return
      end
      abort = @proc_collect.call(@step,@pages[@step].widget) unless @proc_collect.nil?
      # Don't move to the next page, if false is returned
      unless abort
        return
      end
      @pages[@step].hide

      # Ensure Previous button is properly enabled / disabled
      if @step + 1 == 1
        @bprev.enable(true)
      end

      # Incase proc_collect set skip on a page, move to the correct page
      @step += 1
      while true
        if @step > @pages.length - 1
          @step -= 1
          break
        end
        break unless @pages[@step].skip?
        @step += 1
      end

      # Detect end of Wizard, and call wizard_end proc, and detect last page, and set finish text
      if @step == @pages.length - 1
        @bnext.set_label(@finish_text)
      end

      # We got here, so show our page
      @pages[@step].show
      @step_change.call(@step) unless @step_change.nil?
    end

    def prev_page()
      if @step == @pages.length - 1
        @bnext.set_label(@next_text)
      elsif @step == 1
        @bprev.enable(false)
      elsif @step == 0
        return
      end

      @pages[@step].hide
      @step -= 1
      while @pages[@step].skip?
        @step -= 1
      end
      @pages[@step].show
      @step_change.call(@step) unless @step_change.nil?
    end
  end
end
