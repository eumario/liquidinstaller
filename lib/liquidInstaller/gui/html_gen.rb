# Liquid Installer, version 0.1 Alpha
# Copyright (C) 2012 Mario Steele
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detaisl.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# You may contact the original author via email at mario@ruby-im.net


require 'erb'

module LiquidInstaller
  class ProgressHTML
    TEMPLATE_HTML = IO.read(Config.prefix("share/liquidInstaller/step.html"))
    WELCOME_HTML =  IO.read(Config.prefix("share/liquidInstaller/welcome.html"))
    PARTITION_HTML = IO.read(Config.prefix("share/liquidInstaller/partition_list.html"))
    SUMMARY_HTML = IO.read(Config.prefix("share/liquidInstaller/summary.html"))
    SLIDESHOW_HTML = {
        "slideshow_sample" => IO.read(Config.prefix("share/liquidInstaller/slideshow/slideshow_sample.html"))
    }
    PART_COLORS = {
        "unallocated" => "#A9A9A9",
        "unknown" => "#000000",
        "unformatted" => "#000000",
        "extended" => "#7DFCFE",
        "btrfs" => "#FF9955",
        "ext2" => "#9DB8D2",
        "ext3" => "#7590AE",
        "ext4" => "#4B6983",
        "swap" => "#C1665A",
        "fat16" => "#00FF00",
        "vfat" => "#18D918",
        "exfat" => "#2E8B57",
        "nilfs2" => "#826647",
        "ntfs" => "#42E5AC",
        "reiserfs" => "#ADA7C8",
        "reiser4" => "#887FA3",
        "xfs" => "#EED680",
        "jfs" => "#E0C39E",
        "hfs" => "#E0B6AF",
        "hfsplus" => "#C0A39E",
        "ufs" => "#D1940C",
        "used" => "#F8F8BA",
        "unused" => "#FFFFFF",
        "lvm2" => "#CC9966",
        "luks" => "#625B81",
        "default" => "#000000"
    }
    def initialize(installer,setup)
      @installer = installer
      @setup = setup
    end

    def generate_step(step)
      @step = step
      erb = ERB.new(TEMPLATE_HTML)
      return erb.result(binding)
    end

    def generate_welcome()
      erb = ERB.new(WELCOME_HTML)
      return erb.result(binding)
    end

    def generate_partition(partitions)
      erb = ERB.new(PARTITION_HTML)
      @partition = partitions
      return erb.result(binding)
    end

    def generate_summary
      erb = ERB.new(SUMMARY_HTML)
      return erb.result(binding)
    end

    def generate_slideshow(which)
      erb = ERB.new(SLIDESHOW_HTML[which])
      return erb.result(binding)
    end

    private # These methods are only available for generation

    def highlight_step(step)
      if step == @step
        return "<font color='#FFFFFF'><b>"
      elsif step < @step
        return "<font color='#999999'><b>"
      else
        return "<font color='#000000'>"
      end
    end

    def end_highlight(step)
      if step == @step || step < @step
        return "</b></font>"
      else
        return "</font>"
      end
    end

    def lll_name()
      return @installer.distro_name
    end

    def lll_version()
      return "v" + @installer.distro_vers.to_s + " " + @installer.distro_candidate
    end

    def image_url(image)
      return "file://" + Config.prefix("share/liquidInstaller/images",image)
    end

    def tab_html()
      return "&nbsp;" * 8
    end

    def bullet()
      return "file://" + Config.prefix("share/liquidInstaller/images/bullet-point.png")
    end

    def color_part(type)
      if PART_COLORS.has_key? type
        return PART_COLORS[type]
      else
        return PART_COLORS["unknown"]
      end
    end

    def calc_width(partition)
      percent = (partition.size * 100) / partition.dev.size
      if percent < 5
        return 5
      else
        return percent
      end
    end

    def calc_mb(size)
      return size / 1024
    end

    def calc_gb(size)
      return size / 1024 / 1024
    end
  end
end
