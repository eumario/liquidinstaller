# Liquid Installer, version 0.1 Alpha
# Copyright (C) 2012 Mario Steele
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detaisl.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# You may contact the original author via email at mario@ruby-im.net


module Wx
  class XmlResource
    alias :wxload :load

    def load(fname)
      wxload(LiquidInstaller::Config.prefix(fname))
    end
  end

  class Colour
    class << self
      def from_html(html_color)
        r = html_color[1..2]
        g = html_color[3..4]
        b = html_color[5..6]
        a = html_color[7..8] if html_color.length == 9
        if a.nil?
          new(r.to_i(16),g.to_i(16),b.to_i(16))
        else
          new(r.to_i(16),g.to_i(16),b.to_i(16),a.to_i(16))
        end
      end
    end
  end

end
