# Liquid Installer, version 0.1 Alpha
# Copyright (C) 2012 Mario Steele
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detaisl.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# You may contact the original author via email at mario@ruby-im.net



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::InstallerGUI < Wx::Frame
	
	attr_reader :wizardcancel,  :wizardprevious,  :wizardnext
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_frame_subclass(self, parent, "InstallerGUI")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@wizardcancel = finder.call("wizardCancel")
		@wizardprevious = finder.call("wizardPrevious")
		@wizardnext = finder.call("wizardNext")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PageWelcome < Wx::Panel
	
	attr_reader :welcomehtml
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_panel_subclass(self, parent, "pageWelcome")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@welcomehtml = finder.call("welcomeHtml")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PageLanguage < Wx::Panel
	
	attr_reader :languageicon,  :m_statictext1,  :m_statictext33, 
              :languageselect
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_panel_subclass(self, parent, "pageLanguage")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@languageicon = finder.call("languageIcon")
		@m_statictext1 = finder.call("m_staticText1")
		@m_statictext33 = finder.call("m_staticText33")
		@languageselect = finder.call("languageSelect")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PageTimezone < Wx::Panel
	
	attr_reader :timezoneicon,  :m_statictext1,  :m_statictext34, 
              :timezoneselect
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_panel_subclass(self, parent, "pageTimezone")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@timezoneicon = finder.call("timezoneIcon")
		@m_statictext1 = finder.call("m_staticText1")
		@m_statictext34 = finder.call("m_staticText34")
		@timezoneselect = finder.call("timezoneSelect")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PageKeyboard < Wx::Panel
	
	attr_reader :keyboardicon,  :m_statictext1,  :m_statictext35, 
              :m_statictext25,  :modelselect,  :keyboardproper, 
              :keyboardminor,  :m_statictext5,  :keyboardtest
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_panel_subclass(self, parent, "pageKeyboard")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@keyboardicon = finder.call("keyboardIcon")
		@m_statictext1 = finder.call("m_staticText1")
		@m_statictext35 = finder.call("m_staticText35")
		@m_statictext25 = finder.call("m_staticText25")
		@modelselect = finder.call("modelSelect")
		@keyboardproper = finder.call("keyboardProper")
		@keyboardminor = finder.call("keyboardMinor")
		@m_statictext5 = finder.call("m_staticText5")
		@keyboardtest = finder.call("keyboardTest")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PageSelectDisk < Wx::Panel
	
	attr_reader :diskicon,  :m_statictext1,  :m_statictext36, 
              :diskselect
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_panel_subclass(self, parent, "pageSelectDisk")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@diskicon = finder.call("diskIcon")
		@m_statictext1 = finder.call("m_staticText1")
		@m_statictext36 = finder.call("m_staticText36")
		@diskselect = finder.call("diskSelect")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PageDisk < Wx::Panel
	
	attr_reader :diskicon,  :m_statictext1,  :m_statictext37, 
              :partitionmethod,  :autopartitionmethod
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_panel_subclass(self, parent, "pageDisk")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@diskicon = finder.call("diskIcon")
		@m_statictext1 = finder.call("m_staticText1")
		@m_statictext37 = finder.call("m_staticText37")
		@partitionmethod = finder.call("partitionMethod")
		@autopartitionmethod = finder.call("autoPartitionMethod")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PagePartitionEditor < Wx::Panel
	
	attr_reader :diskicon,  :m_statictext1,  :m_statictext32, 
              :m_statictext24,  :driveselect,  :partitionview, 
              :partitionlist,  :editpartition,  :launchgparted
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_panel_subclass(self, parent, "pagePartitionEditor")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@diskicon = finder.call("diskIcon")
		@m_statictext1 = finder.call("m_staticText1")
		@m_statictext32 = finder.call("m_staticText32")
		@m_statictext24 = finder.call("m_staticText24")
		@driveselect = finder.call("driveSelect")
		@partitionview = finder.call("partitionView")
		@partitionlist = finder.call("partitionList")
		@editpartition = finder.call("editPartition")
		@launchgparted = finder.call("launchGparted")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PartitionEditDlg < Wx::Dialog
	
	attr_reader :m_statictext25,  :partition_path,  :m_statictext27, 
              :partition_type,  :m_statictext42,  :partition_size, 
              :m_statictext29,  :mount_type,  :format_partition, 
              :m_statictext31,  :mount_point
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_dialog_subclass(self, parent, "PartitionEditDlg")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@m_statictext25 = finder.call("m_staticText25")
		@partition_path = finder.call("partition_path")
		@m_statictext27 = finder.call("m_staticText27")
		@partition_type = finder.call("partition_type")
		@m_statictext42 = finder.call("m_staticText42")
		@partition_size = finder.call("partition_size")
		@m_statictext29 = finder.call("m_staticText29")
		@mount_type = finder.call("mount_type")
		@format_partition = finder.call("format_partition")
		@m_statictext31 = finder.call("m_staticText31")
		@mount_point = finder.call("mount_point")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PageUser < Wx::Panel
	
	attr_reader :usericon,  :m_statictext1,  :m_statictext38, 
              :m_statictext26,  :real_name,  :m_statictext27, 
              :computer_name,  :m_statictext30,  :m_statictext28, 
              :user_name,  :m_statictext31,  :user_password, 
              :password_strength,  :m_statictext32, 
              :user_password_confirm,  :password_match, 
              :login_automatically,  :require_password
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_panel_subclass(self, parent, "pageUser")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@usericon = finder.call("userIcon")
		@m_statictext1 = finder.call("m_staticText1")
		@m_statictext38 = finder.call("m_staticText38")
		@m_statictext26 = finder.call("m_staticText26")
		@real_name = finder.call("real_name")
		@m_statictext27 = finder.call("m_staticText27")
		@computer_name = finder.call("computer_name")
		@m_statictext30 = finder.call("m_staticText30")
		@m_statictext28 = finder.call("m_staticText28")
		@user_name = finder.call("user_name")
		@m_statictext31 = finder.call("m_staticText31")
		@user_password = finder.call("user_password")
		@password_strength = finder.call("password_strength")
		@m_statictext32 = finder.call("m_staticText32")
		@user_password_confirm = finder.call("user_password_confirm")
		@password_match = finder.call("password_match")
		@login_automatically = finder.call("login_automatically")
		@require_password = finder.call("require_password")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PageGrub < Wx::Panel
	
	attr_reader :diskicon,  :m_statictext1,  :m_statictext30, 
              :m_statictext32,  :diskselect,  :noinstall, 
              :m_statictext39,  :m_statictext40,  :root_password, 
              :password_strength,  :m_statictext41, 
              :root_password_confirm,  :password_match
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_panel_subclass(self, parent, "pageGrub")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@diskicon = finder.call("diskIcon")
		@m_statictext1 = finder.call("m_staticText1")
		@m_statictext30 = finder.call("m_staticText30")
		@m_statictext32 = finder.call("m_staticText32")
		@diskselect = finder.call("diskSelect")
		@noinstall = finder.call("noInstall")
		@m_statictext39 = finder.call("m_staticText39")
		@m_statictext40 = finder.call("m_staticText40")
		@root_password = finder.call("root_password")
		@password_strength = finder.call("password_strength")
		@m_statictext41 = finder.call("m_staticText41")
		@root_password_confirm = finder.call("root_password_confirm")
		@password_match = finder.call("password_match")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PageSummary < Wx::Panel
	
	attr_reader :summaryicon,  :m_statictext1,  :m_statictext41, 
              :summaryhtml
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_panel_subclass(self, parent, "pageSummary")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@summaryicon = finder.call("summaryIcon")
		@m_statictext1 = finder.call("m_staticText1")
		@m_statictext41 = finder.call("m_staticText41")
		@summaryhtml = finder.call("summaryHTML")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PageInstalling < Wx::Panel
	
	attr_reader :installicon,  :m_statictext1,  :slideshowhtml, 
              :installprogress,  :progresstext
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_panel_subclass(self, parent, "pageInstalling")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@installicon = finder.call("installIcon")
		@m_statictext1 = finder.call("m_staticText1")
		@slideshowhtml = finder.call("slideshowHtml")
		@installprogress = finder.call("installProgress")
		@progresstext = finder.call("progressText")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end



# This class was automatically generated from XRC source. It is not
# recommended that this file is edited directly; instead, inherit from
# this class and extend its behaviour there.  
#
# Source file: share/liquidInstaller/LiquidGUI.xrc 
# Generated at: 2012-01-10 23:40:53 -0500

class LiquidInstaller::PageFinished < Wx::Panel
	
	attr_reader :finishedicon,  :m_statictext1,  :finishhtml
	
	def initialize(parent = nil)
		super()
		xml = Wx::XmlResource.get
		xml.flags = 2 # Wx::XRC_NO_SUBCLASSING
		xml.init_all_handlers
		xml.load("share/liquidInstaller/LiquidGUI.xrc")
		xml.load_panel_subclass(self, parent, "pageFinished")

		finder = lambda do | x | 
			int_id = Wx::xrcid(x)
			begin
				Wx::Window.find_window_by_id(int_id, self) || int_id
			# Temporary hack to work around regression in 1.9.2; remove
			# begin/rescue clause in later versions
			rescue RuntimeError
				int_id
			end
		end
		
		@finishedicon = finder.call("finishedIcon")
		@m_statictext1 = finder.call("m_staticText1")
		@finishhtml = finder.call("finishHTML")
		if self.class.method_defined? "on_init"
			self.on_init()
		end
	end
end


