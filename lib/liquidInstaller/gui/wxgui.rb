# Liquid Installer, version 0.1 Alpha
# Copyright (C) 2012 Mario Steele
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detaisl.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# You may contact the original author via email at mario@ruby-im.net


### GUI Toolkit libraries
require 'wx'

### System Libraries
require 'rexml/document'
require 'net/http'
require 'geoip'

### Core Installer libraries
require 'lib/liquidInstaller/core/rubyparted'
require 'lib/liquidInstaller/core/utils'
require 'lib/liquidInstaller/core/config'
require 'lib/liquidInstaller/core/installer'
require 'lib/liquidInstaller/core/validation'

### GUI Installer libraries
require 'lib/liquidInstaller/gui/wxutils'
require 'lib/liquidInstaller/gui/LiquidGUI'
require 'lib/liquidInstaller/gui/html_gen'
require 'lib/liquidInstaller/gui/wizard'

module LiquidInstaller
  class GUI < InstallerGUI
    GEOIP_URL = "http://www.liquidlemur.com/installer/whatismyip.php"
    MKFS_SUPPORT = {
        "btrfs" => "/sbin/mkfs.btrfs",
        "exfat" => "/sbin/mkfs.exfat",
        "ext2" => "/sbin/mkfs.ext2",
        "ext3" => "/sbin/mkfs.ext3",
        "ext4" => "/sbin/mkfs.ext4",
        "fat16" => "/sbin/mkfs.vfat",
        "fat32" => "/sbin/mkfs.vfat",
        "hfsplus" => "/sbin/mkfs.hfsplus",
        "jfs" => "/sbin/mkfs.jfs",
        "swap" => "/sbin/mkswap",
        "ntfs" => "/sbin/mkfs.ntfs",
        "reiser4" => "/sbin/mkfs.reiser4",
        "reiserfs" => "/sbin/mkfs.reiserfs",
        "xfs" => "/sbin/mkfs.xfs"
    }
    def initialize
      super

      ### DOC: Setup Installer configuration template
      @setup = Setup.new
      @installer = Installer.new
      @disks = Parted.probeDisks
      @ssTimer = Wx::Timer.new(self,1000)
      @pulseTimer = Wx::Timer.new(self,1001)
      @threadTimer = Wx::Timer.new(self,1002)

      ### DOC: Get Sizers to be used
      @sizer = self.get_sizer.get_children[0].get_sizer

      ### DOC: Create Panels to be used in the Installer
      @progress = PageWelcome.new(self) # For Progress report
      @wizard = LIWizard.new(@wizardprevious,@wizardnext)
      # Welcome Introduction
      @wizard.add_page(LIWizardPage.new(PageWelcome.new(self),@sizer),:pw)
      # Language Selection
      @wizard.add_page(LIWizardPage.new(PageLanguage.new(self),@sizer))
      # Timezone Selection
      @wizard.add_page(LIWizardPage.new(PageTimezone.new(self),@sizer))
      # Keyboard Selection
      @wizard.add_page(LIWizardPage.new(PageKeyboard.new(self),@sizer))
      # Select a Disk
      @wizard.add_page(LIWizardPage.new(PageSelectDisk.new(self),@sizer))
      # Select Partitioning Method
      @wizard.add_page(LIWizardPage.new(PageDisk.new(self),@sizer))
      # Select Disk Partition Editor
      @wizard.add_page(LIWizardPage.new(PagePartitionEditor.new(self),@sizer),:pe)
      # Enter User Information
      @wizard.add_page(LIWizardPage.new(PageUser.new(self),@sizer))
      # GRUB Installation
      @wizard.add_page(LIWizardPage.new(PageGrub.new(self),@sizer))
      # Summary of Setup
      @wizard.add_page(LIWizardPage.new(PageSummary.new(self),@sizer),:ss)
      # Progress of Installation
      @wizard.add_page(LIWizardPage.new(PageInstalling.new(self),@sizer),:pip)
      # Thank you message
      @wizard.add_page(LIWizardPage.new(PageFinished.new(self),@sizer))
      @wizard.finish_text = "&Reboot"

      # Setup Event Handlers
      evt_button(@wizardprevious) do
        @wizard.prev_page()
      end

      evt_button(@wizardnext) do
        @wizard.next_page()
      end
      @wizard.proc_collect = proc {|step,widget| collect_info(step,widget)}
      @wizard.step_change = proc {|step| @progress.welcomehtml.set_page(@phtml.generate_step(step))}
      @wizard.wizard_end = proc { self.close(true) }

      evt_button(@wizardcancel) do
        mb = Wx::MessageDialog.new(self,"Are you sure you want to abort the installation?","Quit Liquid Lemur Linux Installation",Wx::YES_NO|Wx::NO_DEFAULT)
        if mb.show_modal == Wx::ID_YES
          close(true)
        end
      end

      # Setup Controls
      @phtml = ProgressHTML.new(@installer,@setup)
      @sizer.add(@progress,1,Wx::ALL|Wx::EXPAND)
      @wizardprevious.enable(false)
      @wizard[:pw].widget.welcomehtml.set_page(@phtml.generate_welcome)
      @wizard[:pe].skip
      initialize_contents


      # Setup our First Step
      @wizard.init_page()

      @installer.progress_hook do |type,msg,curr,total|
        progress_call(type,msg,curr,total)
      end

      @installer.question_box do |title, msg, buttons|
        message_box(title,msg,buttons << :question)
      end
      
      @installer.keep_alive do
        Wx::THE_APP.yield
      end

      evt_timer(1001) do
        @wizard[:pip].widget.installprogress.pulse()
      end

      evt_timer(1002) do
        Thread.pass
      end

      # Set our minimal size
      set_client_size(800,600)
      set_min_size([800,600])
      set_size([800,600])
      @progress.show
    end

    def message_box(title,message,options)
      wxopts = 0
      options.each do |opt|
        case opt
          when :ok
            wxopts |= Wx::OK
          when :cancel
            wxopts |= Wx::CANCEL
          when :yesno
            wxopts |= Wx::YES_NO
          when :yesdef
            wxopts |= Wx::YES_DEFAULT
          when :nodef
            wxopts |= Wx::NO_DEFAULT
          when :exclaim
            wxopts |= Wx::ICON_EXCLAMATION
          when :error
            wxopts |= Wx::ICON_ERROR
          when :question
            wxopts |= Wx::ICON_QUESTION
          when :information
            wxopts |= Wx::ICON_INFORMATION
        end
      end
      return Wx::MessageDialog.new(self,message,title,wxopts).show_modal()
    end

    def progress_call(type, message, current, total)
      page = @wizard[:pip]
      pb = page.widget.installprogress
      pt = page.widget.progresstext
      pb.set_range(total) unless pb.get_range == total
      case type
        when :start
          start_slide_show
          pt.set_label(message)
          pb.set_value(current)
          @wizardprevious.enable(false)
          @wizardnext.enable(false)
        when :pulse_start
          pt.set_label(message)
          @pulseTimer.start(100)
        when :pulse
          pt.set_label(message)
        when :pulse_stop
          @pulseTimer.stop()
        when :update
          pt.set_label(message)
          pb.set_value(current)
        when :complete
          @success = true
          @wizard.next_page
          @wizardnext.enable
        when :error
          @success = false
          @error = message
          @wizard.next_page
          @wizardnext.enable
      end
    end

    def collect_info(step,widget)
      ### TODO: Collect information from pages
      ### DOC: Step 0, 9, 10 and 11 are information steps, not collecting data steps.
      case step
        when 0

        when 1    # Language
          li = widget.languageselect.get_selections()[0]
          if li.nil?
            message_box("Language Selection Error","Please select a language before continuing",[:error,:ok])
            return false
          end
          li = widget.languageselect.get_item(li)
          desc = li.get_text()
          @setup.language =  @langreg[desc]
        when 2    # Time Zone
          ti = widget.timezoneselect.get_selection()
          if ti == @tzroot
            message_box("Timezone Selection Error","Please select a timezone before continuing",[:error,:ok])
            return false
          end
          if widget.timezoneselect.get_item_parent(ti) == @tzroot
            message_box("Timezone Selection Error","Please select a timezone before continuing",[:error,:ok])
            return false
          end
          country = widget.timezoneselect.get_item_text(widget.timezoneselect.get_item_parent(ti))
          timezone = widget.timezoneselect.get_item_text(ti).split(" ").join("_")
          @setup.timezone[:timezone] = "#{country}/#{timezone}"
          pi = widget.timezoneselect.get_item_parent(ti)
          @setup.timezone[:timezone_code] = @tzreg["#{country}/#{timezone}"]
        when 3    # Keyboard Selection
          model_desc = widget.modelselect.get_string_selection()
          if model_desc == ""
            message_box("Keyboard Selection Error","Please select the Model of keyboard to use before continuing",[:error,:ok])
            return false
          end
          model = widget.modelselect.get_item_data(widget.modelselect.get_selection())
          layout_desc = widget.keyboardproper.get_selections()[0]
          if layout_desc.nil?
            message_box("Keyboard Selection Error","Please select the Layout of the keyboard to use before continuing",[:error,:ok])
            return false
          end
          layout_desc = widget.keyboardproper.get_item_text(layout_desc)
          variant_desc = widget.keyboardminor.get_selections()[0]
          variant_desc = widget.keyboardminor.get_item_text(variant_desc) unless variant_desc.nil?
          layout = nil
          variant = nil
          @variants.each do |name,variants|
            if variants[:desc] == layout_desc
              layout = name
              variants[:variants].each do |varname, vardesc|
                if vardesc == variant_desc
                  variant_name = varname
                  break
                end
              end unless variant_desc.nil?
              break unless layout.nil?
            end
          end
          @setup.keyboard[:model] = model
          @setup.keyboard[:model_desc] = model_desc
          @setup.keyboard[:variant] = variant.nil? ? "" : variant
          @setup.keyboard[:variant_desc] = variant_desc.nil? ? "" : variant_desc
          @setup.keyboard[:layout] = layout
          @setup.keyboard[:layout_desc] = layout_desc
        when 4    # Disk Selection
          li = widget.diskselect.get_selections()[0]
          if li.nil?
            message_box("Disk Selection Error","Please select a disk in which to install Liquid Lemur Linux on",[:error,:ok])
            return false
          end
          li = widget.diskselect.get_item(li)
          @setup.disk = li.get_text().split("(")[0].strip
          # Ensure that Advanced Partition is properly setup
          ds = @wizard[:pe].widget.driveselect
          pv = @wizard[:pe].widget.partitionview
          pl = @wizard[:pe].widget.partitionlist
          ds.each do |item|
            if ds.get_string(item) == @setup.disk
              ds.select(item)
            end
          end
          build_partition_view(pv,pl,ds)
        when 5    # Partition method
          unless @setup.advanced_partition
            val = widget.autopartitionmethod.get_selection
            if val == 0
              @setup.partition_table = {}
              @setup.partition_table[@setup.disk + "1"] = {:format_type => "ext4", :mount_point => "/", :format => true}
              @setup.partition_table[@setup.disk + "2"] = {:format_type => "swap", :mount_point => "swap", :format => true}
            else
              @setup.partition_table = {}
              @setup.partition_table[@setup.disk + "1"] = {:format_type => "ext4", :mount_point => "/", :format => true}
              @setup.partition_table[@setup.disk + "2"] = {:format_type => "ext4", :mount_point => "/home", :format => true}
              @setup.partition_table[@setup.disk + "3"] = {:format_type => "swap", :mount_point => "swap", :format => true}
            end
          end
        when 6    # Advanced Partition
          root_chosen = false
          swap_chosen = false
          @disks.each do |disk|
            next if disk.removable?
            part = disk.getFirstPartition
            while !part.nil?
              if !part.mount.empty?
                @setup.partition_table[part.path] = {:format_type=>part.format_as,:mount_point=>part.mount,:format=>part.format?}
                if part.mount == "/"
                  root_chosen = true
                elsif part.mount == "swap"
                  swap_chosen = true
                end
              end
              part = part.nextPartition
            end
          end
          if !root_chosen
            message_box("No Root Selected","Please select a partition for Root, before continuing",[:ok,:error])
            return false
          end
          if !swap_chosen
            res = message_box("No Swap Selected","It is not required to choose swap, but it is recommended to have a swap partition setup.\nDo you wish to continue without a swap partition?",[:question,:yesno])
            if res == Wx::ID_NO
              return false
            end
          end
        when 7    # User Info
          @setup.user_info[:realname] = widget.real_name.get_value
          @setup.user_info[:hostname] = widget.computer_name.get_value
          @setup.user_info[:username] = widget.user_name.get_value
          @setup.user_info[:pass1] = widget.user_password.get_value
          @setup.user_info[:pass2] = widget.user_password_confirm.get_value
          @setup.user_info[:autologin] = widget.login_automatically.get_value
          res = Validation.validate_hostname(@setup.user_info[:hostname])
          if not res.empty?
            if res == "invalid_length"
              res = "Hostname must be between 1 and 63 characters in length"
            elsif res == "bad_char"
              res = "Hostname can only contain letters, numbers, dashes and dots"
            elsif res == "bad_hyphen"
              res = "Hostname cannot start with, or end with a hyphen"
            elsif res == "bad_dots"
              res = "Hostname cannot start with, end with, or have 2 dots together in it"
            end
            message_box("Invalid Hostname",res,[:error,:ok])
            return false
          end
          if @setup.user_info[:username].nil? or @setup.user_info[:username].empty?
            message_box("Username Not Provided","You must provide a username in which to login with on your computer",[:error,:ok])
            return false
          end
          res = Validation.validate_username(@setup.user_info[:username])
          if !res.empty?
            if res == "first_lowercase"
              res = "Username must start with a lower-case letter."
            elsif res == "invalid_symbols"
              res = "Username may only contain lower-case letters, digits, hypens and underscores"
            end
            message_box("Invalid Username",res,[:error,:ok])
            return false
          end
          if @setup.user_info[:pass1].empty?
            message_box("Invalid Password","No password is given, please enter a password before continuing.",[:error,:ok])
          end
          unless (@setup.user_info[:pass1] == @setup.user_info[:pass2])
            message_box("Invalid Password","Invalid password.  The Confirmation of the password does not match the original, please try again.",[:error,:ok])
            return false
          end
        when 8    # Grub and Root
          @setup.bootloader[:devid] = widget.diskselect.get_string_selection
          @setup.bootloader[:install] = widget.noinstall.is_checked ? false : true
          @setup.root[:password] = widget.root_password.get_value
          @setup.root[:password_confirm] = widget.root_password_confirm.get_value
          if @setup.root[:password] != @setup.root[:password_confirm]
            message_box("Invalid Password","Invalid password.  The confirmation of the password does not match the original, please try again.",[:error,:ok])
            return false
          end
          if @setup.root[:password].empty?
            message_box("Invalid Password","No password is given, please enter a password before continuing.",[:error,:ok])
          end
          @wizard[:ss].widget.summaryhtml.set_page(@phtml.generate_summary)
        when 9    # Installation Progress
          Thread.new do
            @threadTimer.start(25)
            @installer.run_install(@setup)
            @threadTimer.stop
          end
      end
      return true
    end

    def start_slide_show()
      @wizard[:pip].widget.slideshowhtml.set_page(@phtml.generate_slideshow("slideshow_sample"))
    end

    def initialize_contents
      @wizard.pages.each_with_index do |page,i|
        case i
          when 0
            page.widget.welcomehtml.enable
          when 1
            initialize_language(page.widget)
            page.widget.languageicon.set_bitmap(Wx::Bitmap.new(Config.prefix("share/liquidInstaller/images/icons/locales.png"),Wx::BITMAP_TYPE_PNG))
          when 2
            initialize_timezone(page.widget)
            page.widget.timezoneicon.set_bitmap(Wx::Bitmap.new(Config.prefix("share/liquidInstaller/images/icons/time.png"),Wx::BITMAP_TYPE_PNG))
          when 3
            initialize_keyboard(page.widget)
            page.widget.keyboardicon.set_bitmap(Wx::Bitmap.new(Config.prefix("share/liquidInstaller/images/icons/keyboard.png"),Wx::BITMAP_TYPE_PNG))
          when 4
            initialize_diskselect(page.widget)
            page.widget.diskicon.set_bitmap(Wx::Bitmap.new(Config.prefix("share/liquidInstaller/images/icons/hdd.png"),Wx::BITMAP_TYPE_PNG))
          when 5
            page.widget.diskicon.set_bitmap(Wx::Bitmap.new(Config.prefix("share/liquidInstaller/images/icons/hdd.png"),Wx::BITMAP_TYPE_PNG))
            evt_radiobox(page.widget.partitionmethod) do |event|
              indx = event.get_selection()
              if indx == 0
                page.widget.autopartitionmethod.enable
                @setup.advanced_partition = false
                @wizard[6].skip
              else
                page.widget.autopartitionmethod.enable false
                @setup.advanced_partition = true
                @wizard[6].unskip
              end
              @progress.welcomehtml.set_page(@phtml.generate_step(5))
            end
          when 6
            initialize_partitioneditor(page.widget)
            page.widget.diskicon.set_bitmap(Wx::Bitmap.new(Config.prefix("share/liquidInstaller/images/icons/hdd.png"),Wx::BITMAP_TYPE_PNG))
          when 7
            initialize_userinfo(page.widget)
            page.widget.usericon.set_bitmap(Wx::Bitmap.new(Config.prefix("share/liquidInstaller/images/icons/user.png"),Wx::BITMAP_TYPE_PNG))
          when 8
            initialize_grubselect(page.widget)
            page.widget.diskicon.set_bitmap(Wx::Bitmap.new(Config.prefix("share/liquidInstaller/images/icons/advanced.png"),Wx::BITMAP_TYPE_PNG))
          when 9
            page.widget.summaryicon.set_bitmap(Wx::Bitmap.new(Config.prefix("share/liquidInstaller/images/icons/summary.png"),Wx::BITMAP_TYPE_PNG))
          when 10
            page.widget.installicon.set_bitmap(Wx::Bitmap.new(Config.prefix("share/liquidInstaller/images/icons/install.png"),Wx::BITMAP_TYPE_PNG))
          when 11
            page.widget.finishedicon.set_bitmap(Wx::Bitmap.new(Config.prefix("share/liquidInstaller/images/icons/finished.png"),Wx::BITMAP_TYPE_PNG))
        end
      end
    end

    def initialize_language(widget)
      ls = widget.languageselect

      ## Attempt to get our GeoIP
      begin
        ip_addr = Net::HTTP.get(URI.parse(GEOIP_URL))
        gi = GeoIP.new(Config.prefix("share/liquidInstaller/GeoIP.dat"))
        geo = gi.country(ip_addr)
        geo = geo[3]
      rescue
        geo = nil
      end

      if geo.nil?  # We fall back to secondary access
        cur_lang = ENV["LANG"].split(".")[0]
      end

      il = Wx::ImageList.new(16,16)
      @ilreg = {}
      files = Dir[Config.prefix("share/liquidInstaller/images/flags/16/*.png")]
      files.each do |file|
        @ilreg[File.basename(file,".png")] = il.add(Wx::Bitmap.new(file))
      end
      ls.set_image_list(il,Wx::IMAGE_LIST_SMALL)
      ls.insert_column(0,"language",Wx::LIST_FORMAT_LEFT,500)

      countries = {}
      languages = {}
      @langreg = {}
      File.open(Config.prefix("share/liquidInstaller/countries")) do |fh|
        fh.each_line do |line|
          split = line.strip.rstrip.split("=")
          if split.length == 2
            countries[split[0]] = split[1]
          end
        end
      end

      File.open(Config.prefix("share/liquidInstaller/languages")) do |fh|
        fh.each_line do |line|
          split = line.strip.rstrip.split("=")
          if split.length == 2
            languages[split[0]] = split[1]
          end
        end
      end

      cur_index = -1
      set_index = nil

      File.open(Config.prefix("share/liquidInstaller/locales")) do |fh|
        fh.each_line do |line|
          cur_index += 1
          if line.index("UTF-8")
            locale_code = line.gsub(/UTF\-8/,"").gsub(/\./,"").strip.rstrip
            if locale_code.index("_")
              split = locale_code.split("_")
              if split.length == 2
                language_code = split[0]
                if languages.has_key? language_code
                  language = languages[language_code]
                else
                  language = language_code
                end

                country_code = split[1].downcase
                if countries.has_key? country_code
                  country = countries[country_code]
                else
                  country = country_code
                end

                language_label = "%s (%s)" % [language,country]
                @langreg[language_label] = locale_code
                if @ilreg.has_key? country_code
                  flag = @ilreg[country_code]
                else
                  flag = @ilreg["generic"]
                end
                indx = ls.insert_item(cur_index,language_label,flag)

                # Plan A - By GeoIP Address lookup
                if ((!geo.nil?) && (geo.downcase == country_code))
                  if set_index.nil?
                    set_index = indx
                  else
                    if (language_code == "en")
                      set_index = indx
                    elsif (country_code == language_code)
                      set_index = indx
                    end
                  end
                end

                # Plan B - using the locale for USA
                if ((set_index.nil?) && (locale_code == cur_lang))
                  set_index = indx
                end

                if indx % 2 == 1
                  ls.set_item_background_colour(indx,Wx::Colour.new(168,168,168))
                end
              end
            end
          end
        end
      end


      ls.sort_items {|x,y| x <=> y}

      if set_index
        li = ls.get_item(set_index)
        li.set_state(Wx::LIST_STATE_SELECTED)
        ls.set_item(li)
        ls.ensure_visible(set_index)
      end
    end

    def initialize_timezone(widget)
      tzs = widget.timezoneselect
      @tzroot = tzs.add_root("Timezones")
      @tzreg = {}
      roots = {}
      File.open(Config.prefix("share/liquidInstaller/timezones")) do |fh|
        fh.each_line do |line|
          content = line.strip().split()
          if content.length == 2
            country_code = content[0]
            timezone = content[1]
            country_name = timezone.split("/")[0]
            timezone_name = timezone.split("/")[1..-1].join("/").split("_").join(" ")
            if roots.has_key? country_name
              root = roots[country_name]
            else
              root = tzs.append_item(@tzroot,country_name)
              roots[country_name] = root
            end
            tzs.append_item(root,timezone_name)
            @tzreg[timezone] = country_code
          end
        end
      end
      roots.each do |country_name,root|
        tzs.sort_children(root)
      end
    end

    def initialize_keyboard(widget)
      kbp = widget.keyboardproper
      kbm = widget.keyboardminor
      ms = widget.modelselect

      kbp.insert_column(0,"Layout",Wx::LIST_FORMAT_LEFT,230)
      kbm.insert_column(0,"Variant",Wx::LIST_FORMAT_LEFT,230)

      keyboard_layout = nil
      keyboard_geom = nil


      # Figure out our Keyboard Maps
      data = `setxkbmap -print`
      data.each_line do |line|
        line = line.rstrip()
        line = line.gsub(/\{/,"")
        line = line.gsub(/\}/,"")
        line = line.gsub(/;/,"")
        if line.index("xkb_symbols")
          section = line.split("\"")[1]
          keyboard_layout = section.split("+")[1]
        elsif line.index("xkb_geometry")
          first_bracket = line.index("(") + 1
          substr = line[first_bracket..-1]
          last_bracket = substr.index(")") - 1
          substr = substr[0..last_bracket]
          keyboard_geom = substr
        end
      end

      doc = REXML::Document.new File.new("/usr/share/X11/xkb/rules/xorg.xml")

      # If we find the users keyboard info we can set it in the list
      set_keyboard_model = nil
      set_keyboard_layout = nil

      # Generate Model Select
      cur_item = -1
      doc.elements.each("xkbConfigRegistry/modelList/model/configItem") do |element|
        cur_item += 1
        name = ""
        desc = ""
        element.elements.each("name") {|x| name = x.text}
        element.elements.each("description") {|x| desc = x.text}
        indx = ms.append(desc,name)
        if name == keyboard_geom
          set_keyboard_model = indx
        end
      end

      # Generate Layout List and Variant List
      cur_item = -1
      @variants = {}
      doc.elements.each("xkbConfigRegistry/layoutList/layout") do |element|
        cur_item += 1
        name = ""
        desc = ""
        element.elements.each("configItem/name") {|x| name = x.text}
        element.elements.each("configItem/description") {|x| desc = x.text}
        @variants[name] = {:desc=>desc,:variants=>{}}
        indx = kbp.insert_item(cur_item,desc)
        if name == keyboard_layout
          set_keyboard_layout = indx
        end

        element.elements.each("variantList/variant") do |elem|
          varname = ""
          vardesc = ""
          elem.elements.each("configItem/name") {|x| varname = x.text}
          elem.elements.each("configItem/description") {|x| vardesc = x.text}
          @variants[name][:variants][varname] = vardesc
        end
      end

      evt_choice(ms) do |event|
        indx = evt.get_index()
        model = ms.get_item_data(indx)
        `setxkbmap -model #{model}`
      end

      evt_list_item_selected(kbp) do |event|
        desc = event.get_text()
        kbm.delete_all_items()
        @variants.each do |name, variants|
          if variants[:desc] == desc
            `setxkbmap -layout #{name}`
            cur_item = -1
            variants[:variants].each do |varname,vardesc|
              cur_item += 1
              kbm.insert_item(cur_item,vardesc)
            end
          end
        end
      end

      evt_list_item_selected(kbm) do |event|
        desc = event.get_text()
        sel = kbp.get_selections()[0]
        li = kbp.get_item(sel,0)
        vdesc = li.get_text()
        @variants.each do |name, variants|
          if variants[:desc] == vdesc
            variants[:variants].each do |varname, vardesc|
              if vardesc == desc
                `setxkbmap -variant #{varname}`
              end
            end
          end
        end
      end

      ms.set_selection(set_keyboard_model) if set_keyboard_model
      kbp.sort_items {|x,y| x <=> y }
      if set_keyboard_layout
        li = kbp.get_item(set_keyboard_layout)
        li.set_state(Wx::LIST_STATE_SELECTED)
        kbp.set_item(li)
        kbp.ensure_visible(set_keyboard_layout)
      end
    end

    def initialize_diskselect(widget)
      ds = widget.diskselect

      il = Wx::ImageList.new(16,16)
      hdd = il.add(Wx::Bitmap.new(Config.prefix("share/liquidInstaller/images/icons/hdd16.png"),Wx::BITMAP_TYPE_PNG))
      ds.set_image_list(il,Wx::IMAGE_LIST_SMALL)
      ds.insert_column(0,"disks",Wx::LIST_FORMAT_LEFT,500)

      cur_item = -1
      @disks.each do |drive|
        next if drive.removable?
        cur_item += 1
        if drive.size > 1024
          sdrive = "%s (%s %s)" % [drive.path,drive.size / 1024, "GB"]
        else
          sdrive = "%s (%s %s)" % [drive.path,drive.size, "MB"]
        end
        ds.insert_item(cur_item,sdrive,hdd)
      end
    end

    def initialize_partitioneditor(widget)
      ds = widget.driveselect
      pv = widget.partitionview
      pl = widget.partitionlist

      pl.insert_column(0,"Device",Wx::LIST_FORMAT_LEFT,-1)
      pl.insert_column(1,"Label",Wx::LIST_FORMAT_LEFT,-1)
      pl.insert_column(2,"Type",Wx::LIST_FORMAT_LEFT,-1)
      pl.insert_column(3,"Mount point",Wx::LIST_FORMAT_LEFT,-1)
      pl.insert_column(4,"Format?",Wx::LIST_FORMAT_LEFT,-1)
      pl.insert_column(5,"Size",Wx::LIST_FORMAT_LEFT,-1)

      drive_set_index = nil
      @disks.each do |disk|
        next if disk.removable?
        indx = ds.append(disk.path,disk)
        if disk.device == @setup.disk
          drive_set_index = indx
        end
      end

      ds.set_selection(drive_set_index) if drive_set_index
      build_partition_view(pv,pl,ds)

      evt_choice(ds) do |event|
        build_partition_view(pv,pl,ds)
      end

      evt_button(widget.launchgparted) do
        `gparted #{ds.get_string_selection}`
        ds.get_item_data(ds.get_selection).refreshPartitions
        build_partition_view(pv,pl,ds)
      end

      evt_button(widget.editpartition) do
        if pl.get_selections.empty?
          message_box("No Partition Selection","You must select a partition to edit",[:ok,:error])
        else
          ped = PartitionEditDlg.new(self)
          sel = pl.get_selections[0]
          part = pl.get_item_data(sel)
          ped.partition_path.label = part.path
          ped.partition_type.label = part.type
          if part.size > 1024
            ped.partition_size.label = "#{part.size / 1024}GB"
          else
            ped.partition_size.label = "#{part.size}MB"
          end
          MKFS_SUPPORT.each do |k,v|
            if File.exists?(v)
              indx = ped.mount_type.append(k)
              if k == part.type
                ped.mount_type.select(indx)
              end
            end
          end
          ped.set_affirmative_id(Wx::ID_APPLY)
          if ped.show_modal == Wx::ID_APPLY
            mount_point = ped.mount_point.get_value
            mount_type = ped.mount_type.get_string_selection
            format_to = ped.format_partition.is_checked
            part.mount = mount_point
            part.format_as = mount_type
            part.format = format_to
            build_partition_view(pv,pl,ds)
          end

        end
      end
    end

    def initialize_userinfo(widget)
      up = widget.user_password
      ps = widget.password_strength
      upc = widget.user_password_confirm
      pm = widget.password_match
      evt_text(up) do |event|
        data = up.get_value()
        hint,color = Validation.human_password_strength(data)
        wxcolor = Wx::Colour.from_html(color) unless color == ""
        wxcolor = Wx::BLACK if color == ""
        case hint
          when "too_short"
            label = "Too Short of Password"
          when "weak"
            label = "Weak Password"
          when "fair"
            label = "Fair Password"
          when "good"
            label = "Good Password"
          when "strong"
            label = "Strong Password"
          else
            label = ""
        end
        ps.set_label(label)
        ps.set_foreground_colour(wxcolor)
        ps.refresh
      end
      evt_text(upc) do |event|
        data = upc.get_value()
        data1 = up.get_value()
        if data.empty?
          pm.set_label("")
          pm.set_foreground_colour(Wx::BLACK)
        else
          if data != data1
            pm.set_label("no match")
            pm.set_foreground_colour(Wx::Colour.from_html("#8B0000"))
          else
            pm.set_label("match")
            pm.set_foreground_colour(Wx::Colour.from_html("#006400"))
          end
        end
      end
    end

    def initialize_grubselect(widget)
      ds = widget.diskselect
      ni = widget.noinstall
      up = widget.root_password
      upc = widget.root_password_confirm
      ps = widget.password_strength
      pm = widget.password_match
      @disks.each do |drive|
        ds.append(drive.path) unless drive.removable?
      end

      evt_checkbox(ni) do |event|
        ds.enable(!event.is_checked())
      end
      evt_text(up) do |event|
        data = up.get_value()
        hint,color = Validation.human_password_strength(data)
        wxcolor = Wx::Colour.from_html(color) unless color == ""
        wxcolor = Wx::BLACK if color == ""
        case hint
          when "too_short"
            label = "Too Short of Password"
          when "weak"
            label = "Weak Password"
          when "fair"
            label = "Fair Password"
          when "good"
            label = "Good Password"
          when "strong"
            label = "Strong Password"
          else
            label = ""
        end
        ps.set_label(label)
        ps.set_foreground_colour(wxcolor)
        ps.refresh
      end
      evt_text(upc) do |event|
        data = upc.get_value()
        data1 = up.get_value()
        if data.empty?
          pm.set_label("")
          pm.set_foreground_colour(Wx::BLACK)
        else
          if data != data1
            pm.set_label("no match")
            pm.set_foreground_colour(Wx::Colour.from_html("#8B0000"))
          else
            pm.set_label("match")
            pm.set_foreground_colour(Wx::Colour.from_html("#006400"))
          end
        end
      end
    end

    def build_partition_view(pv,pl,ds)
      indx = ds.get_selection()
      drive = ds.get_item_data(indx)
      part = drive.getFirstPartition
      html = @phtml.generate_partition(part)
      pv.set_page(html)
      pl.delete_all_items()
      i = 0
      while !part.nil?
        if part.type != "unallocated"
          pl.insert_item(i,part.path)
          pl.set_item(i,1,part.label)
          pl.set_item(i,2,part.type)
          pl.set_item(i,3,part.mount)
          pl.set_item(i,4,part.format? ? "Yes" : "No")
          if part.size > 1024
            size_str = (part.size / 1024).to_s + " GB"
          else
            size_str = part.size.to_s + " MB"
          end
          pl.set_item(i,5,size_str)
          pl.set_item_data(i,part)
        end
        part = part.nextPartition
        i += 1
      end
    end

    class << self
      def run
        Wx::App.run do
          Wx::THE_APP.set_app_name("liquidInstaller")
          Wx::THE_APP.set_class_name("liquidInstaller")
          frame = LiquidInstaller::GUI.new
          frame.show
        end
      end
    end
  end
end

if $0 == __FILE__
  LiquidInstaller::GUI.run
end
